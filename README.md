# AWS transcoder support for Transcoder module

## Development notes

Utilizes a multi-value key provider for key module inspired by
[this patch](https://www.drupal.org/files/issues/2806993-8.patch) - this may
eventually move into key module.
