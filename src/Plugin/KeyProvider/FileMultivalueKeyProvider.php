<?php

namespace Drupal\transcoding_aws\Plugin\KeyProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\key\KeyInterface;
use Drupal\key\Plugin\KeyProvider\FileKeyProvider;

/**
 * Adds a key provider that allows a key to be stored in a file.
 *
 * @KeyProvider(
 *   id = "aws_file_multivalue",
 *   label = @Translation("File (Multivalue)"),
 *   description = @Translation("Allows multiple values to be stored in a file, preferably outside of the web root."),
 *   storage_method = "file",
 *   key_value = {
 *     "accepted" = FALSE,
 *     "required" = FALSE
 *   }
 * )
 */
class FileMultivalueKeyProvider extends FileKeyProvider {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['file_format'] = 'JSON';

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['file_format'] = [
      '#type' => 'select',
      '#title' => $this->t('File format'),
      '#description' => $this->t('Select the format of the file.'),
      '#required' => TRUE,
      '#options' => [
        'json' => 'JSON',
      ],
      '#empty_option' => $this->t('- Select format -'),
      '#default_value' => $this->getConfiguration()['file_format'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $key_provider_settings = $form_state->getValues();
    $file = $key_provider_settings['file_location'];

    // Does the file exist?
    if (!is_file($file)) {
      $form_state->setErrorByName('file_location', $this->t('There is no file at the specified location.'));
      return;
    }

    // Is the file readable?
    if ((!is_readable($file))) {
      $form_state->setErrorByName('file_location', $this->t('The file at the specified location is not readable.'));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyValue(KeyInterface $key) {
    $file = $this->configuration['file_location'];
    $format = $this->configuration['file_format'];

    // Make sure the file exists and is readable.
    if (!is_file($file) || !is_readable($file)) {
      return NULL;
    }

    $key_value = file_get_contents($file);

    // Parse the value into multiple values.
    switch ($format) {
      case 'json':
        $key_value = Json::decode($key_value);
        break;
    }

    if (isset($this->configuration['base64_encoded']) && $this->configuration['base64_encoded'] == TRUE) {
      foreach ($key_value as $index => $value) {
        $key_value[$index] = base64_decode($value);
      }
    }

    return $key_value;
  }

}
