<?php

namespace Drupal\transcoding_aws\Events;

final class AwsTranscoderEvents {

  /**
   * Create job event.
   */
  const CREATE_JOB = 'aws_transcoder.create_job';

}
